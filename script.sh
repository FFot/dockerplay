#!/bin/bash

error=0
docker build -t faniweb:100.0 .

docker run -itd --name fanifani -P faniweb:100.0

if ! curl -s localhost:$(docker port fanifani | grep '^80' | awk -F: '{print $NF}') 2>&1 | grep hello
then
	echo "Container failed to work"
	error=1
fi

# Clean up
docker rm -f fanitest
echo "Cleaned container"

if (( error > 0 ))
then
	docker rmi faniweb:100.0
	echo "Image deleted"
fi
